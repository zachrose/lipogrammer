events = require 'events'
CharacterTrackerCollection = require('./character-tracker-collection').CharacterTrackerCollection

class LipogramFinder
  constructor: (chars_of_interest) ->
    @trackers = new CharacterTrackerCollection(chars_of_interest)
    return @
  find_lipogram: (text) ->
    i = 0
    l = text.length
    while i < l
      character = text[i]
      @trackers.accept character
      i++
    @trackers.conclude()

class StreamingLipogramFinder extends events.EventEmitter
  constructor: (chars_of_interest) ->
    @trackers = new CharacterTrackerCollection(chars_of_interest)
    events.EventEmitter.call(this);
    return @
  find_lipogram: (stream) ->
    trackers = @trackers
    stream.on('readable', =>
      chunk = stream.read()
      if (chunk != null)
        i = 0
        while i <= chunk.length
          if(chunk[i])
            @trackers.accept(chunk[i])
          i++
    )
    stream.on 'end', =>
      @emit 'done', @trackers.conclude()

module.exports.LipogramFinder = LipogramFinder
module.exports.StreamingLipogramFinder = StreamingLipogramFinder
