class CharacterTracker

  constructor: (looking_for) ->
    @looking_for = looking_for
    @buffer = ""
    @lipogram = ""

  # public
  accept: (character) ->
    if @_check(character)
      @_nominate_buffer()
      @_reset_buffer()
    else
      @_add character

  conclude: ->
    @_nominate_buffer()
    @lipogram

  # private
  _check: (character) ->
    character.toLowerCase() is @looking_for.toLowerCase()

  _nominate_buffer: (buffer) ->
    @lipogram = @buffer if @buffer.length > @lipogram.length

  _reset_buffer: ->
    @buffer = ""

  _add: (character) ->
    @buffer = @buffer + character

module.exports = {CharacterTracker: CharacterTracker}