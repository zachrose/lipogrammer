CharacterTrackerCollection = require('./character-tracker-collection').CharacterTrackerCollection

class LipogramFinder
  constructor: (chars_of_interest) ->
    @trackers = new CharacterTrackerCollection(chars_of_interest)
    return @
  find_lipogram: (text) ->
    i = 0
    l = text.length
    while i < l
      character = text[i]
      @trackers.accept character
      i++
    @trackers.conclude()

module.exports.LipogramFinder = LipgramFinder