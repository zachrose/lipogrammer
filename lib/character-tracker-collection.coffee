_ = require("underscore")._
CharacterTracker = require('./character-tracker').CharacterTracker

class CharacterTrackerCollection

  constructor: (interests) ->
    @trackers = interests.map (interest) -> new CharacterTracker(interest)
      
  accept: (character) ->
    @trackers.forEach (tracker) -> tracker.accept character

  conclude: ->
    lipograms = @trackers.map (tracker) -> tracker.conclude()
    longest = _(lipograms).max (lipogram) -> lipogram.length
    longest

module.exports.CharacterTrackerCollection = CharacterTrackerCollection