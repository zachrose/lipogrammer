fs = require "fs"
Lipogrammer = require('./lib/lipogrammer').StreamingLipogramFinder

stream = fs.createReadStream("hamlet.txt")
stream.setEncoding('utf8');
scanner = new Lipogrammer(['e','a','t','o'])
scanner.find_lipogram(stream);

expected = """
! Come, some music! come, the recorders!
For if the king like not the comedy,
Why then, belike, he likes it not, perdy.
Come, some music!
Re-enter ROSENCR
"""

scanner.on 'done', (lipogram)->
  if(lipogram == expected)
    console.log('yay')
    process.exit(0)
  else
    console.log('boo')
    process.exit(1)
