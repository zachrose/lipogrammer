Lipogrammer
===========

Wikipedia tells us that a lipogram "is a kind of constrained writing or word game consisting in writing paragraphs or longer works in which a particular letter or group of letters is avoided."

Lipogrammer is a Javascript library that will search for lipograms in a given text or text stream and return the longest lipogram

It has a git repo and a bug tracker. If you're actually using this I'd be interested in hearing about it.